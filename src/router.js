import { createRouter, createWebHashHistory } from 'vue-router'
import Home from './components/Home.vue'
import Settings from './components/Settings.vue'

const routes = [
    {
        path: '/',
        component: Home,
        props: {
            msg: 'Welcome to Hotword Spectrogram building application',
        }
    },
    {
        path: '/settings',
        component: Settings,
    },
];

export const router = createRouter({
    history: createWebHashHistory(),
    routes,
})
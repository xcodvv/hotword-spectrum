import { createStore } from 'vuex'

const state = {
    enumerateDevicesPending: true,
    audioInputDevices: [ ],
    audioInput: { },
}

const mutations = {
    audioInputDevices(state, devices) {
        state.audioInputDevices = devices;
    },
    audioInput(state, info) {
        state.audioInput = info;
    },
    enumerateDevicesPending(state, value) {
        state.enumerateDevicesPending = value;
    }
}

const actions = {
    resolveAudioInputDevices: ({ commit })=>commit('enumerateDevicesPending', false)
}

const getters = {

}

export const settings = createStore({
    state,
    getters,
    actions,
    mutations,
})

const enumerateDevices = ()=>{
    return navigator.mediaDevices.enumerateDevices().then(info=>{
        let defaultDevice = null;
        let devices = [ ];
        info.filter(({kind})=>kind==='audioinput').map(info=>{
            let device = {
                name: info.label,
                code: info.deviceId,
                groupId: info.groupId,
            }
            if (info.deviceId === 'default') {
                defaultDevice = device;
            } else {
                devices.push(device)
            }
        })
        if (defaultDevice) {
            defaultDevice = devices.find(info=>info.groupId===defaultDevice.groupId)
        }
        return { devices, defaultDevice }
    })
}

navigator.mediaDevices.getUserMedia({ audio:true }).then(stream=>{
    stream.getTracks().map(t=>t.stop())

    enumerateDevices().then(({ devices, defaultDevice })=>{
        settings.commit('audioInputDevices', devices)
        settings.commit('audioInput', defaultDevice)
        settings.dispatch('resolveAudioInputDevices')
    })

    navigator.mediaDevices.ondevicechange = (e)=>{
        enumerateDevices().then(({ devices, defaultDevice })=>{
            settings.commit('audioInputDevices', devices)
            let isExist = devices.find(info=>info.groupId===settings.state.audioInput.groupId)
            if (!isExist) {
                settings.commit('audioInput', defaultDevice)
            }
        })
    }
})
import { createApp } from 'vue'
import { router } from './router'
import { settings } from './SettingsStore'
import App from './App.vue'
import IconBox from './components/IconBox.vue'
import PrimeVue from 'primevue/config'
import Dialog from 'primevue/dialog'
import Button from 'primevue/button'
import Dropdown from 'primevue/dropdown'
import TabView from 'primevue/tabview'
import TabPanel from 'primevue/tabpanel'
import ProgressSpinner from 'primevue/progressspinner'
import Message from 'primevue/message'
import Toast from 'primevue/toast'

import 'primevue/resources/themes/saga-blue/theme.css'
import 'primevue/resources/primevue.min.css'
import 'primeflex/primeflex.css'
import 'primeicons/primeicons.css'

const app = createApp(App)

app.use(router)
app.use(settings)

app.use(PrimeVue, { ripple: true })
app.component('IconBox', IconBox)
app.component('Dialog', Dialog)
app.component('Button', Button)
app.component('Dropdown', Dropdown)
app.component('TabView', TabView)
app.component('TabPanel', TabPanel)
app.component('ProgressSpinner', ProgressSpinner)
app.component('Message', Message)
app.component('Toast', Toast)

app.mount('#app')
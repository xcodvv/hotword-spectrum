import { settings } from './SettingsStore'

// settings.subscribe(data=>{
//     if (data.type !== 'audioInput') { return }
//     let { code } = data.payload;
//     // AudioAnalyser.instantiate()
//     // AudioAnalyser.buildGraph(code)
// })

export default class AudioAnalyser {

    static floatTimeDomain = [ ]
    static byteTimeDomain = [ ]
    static byteBuffers = [ ]
    static floatBuffers = [ ]

    static instantiate() {
        this.context = new AudioContext({ latencyHint:'interactive', sampleRate:16000 })
    }

    static buildGraph(deviceId, channelsCount=1) {
        let { context } = this;
        this.splitter = context.createChannelSplitter(channelsCount)
        this.merger = context.createChannelMerger(channelsCount)
        this.volume = context.createGain()
        this.volume.gain.value = 0;
        this.analyser = [ ];
        for(let i=0; i<channelsCount; ++i) {
            let analyser = context.createAnalyser()
            analyser.maxDecibels = -10;
            analyser.minDecibels = -100;
            analyser.fftSize = 512;
            analyser.smoothingTimeConstant = 0.0;
            analyser.connect(this.merger, 0, i)
            this.splitter.connect(analyser, i, 0)
            this.analyser.push(analyser)
        }
        this.merger.connect(this.volume)
        this.volume.connect(context.destination)


        if (this.floatTimeDomain.length > channelsCount) {
            this.floatTimeDomain.splice(0, this.floatTimeDomain.length-channelsCount)
        } else {
            for(let i=this.floatTimeDomain.length; i<channelsCount; ++i) {
                this.floatTimeDomain.push(new Float32Array(this.analyser[i].frequencyBinCount))
            }
        }
        if (this.byteTimeDomain.length > channelsCount) {
            this.byteTimeDomain.splice(0, this.byteTimeDomain.length-channelsCount)
        } else {
            for(let i=this.byteTimeDomain.length; i<channelsCount; ++i) {
                this.byteTimeDomain.push(new Uint8Array(this.analyser[i].frequencyBinCount))
            }
        }

        if (this.floatBuffers.length > channelsCount) {
            this.floatBuffers.splice(0, this.floatBuffers.length-channelsCount)
        } else {
            for(let i=this.floatBuffers.length; i<channelsCount; ++i) {
                this.floatBuffers.push(new Float32Array(this.analyser[i].frequencyBinCount))
            }
        }
        if (this.byteBuffers.length > channelsCount) {
            this.byteBuffers.splice(0, this.byteBuffers.length-channelsCount)
        } else {
            for(let i=this.byteBuffers.length; i<channelsCount; ++i) {
                this.byteBuffers.push(new Uint8Array(this.analyser[i].frequencyBinCount))
            }
        }

        return navigator.mediaDevices.getUserMedia({ audio:{ deviceId } }).then(stream=>{
            this.source = context.createMediaStreamSource(stream)
            this.source.connect(this.splitter)
            context.resume()
            return this
        })
    }

    static disposeGraph() {
        this.context.suspend()
        this.source.mediaStream.getTracks().map(t=>t.stop())
        this.source.disconnect()
    }

    static getFloatTimeDomainData(index=0) {
        this.analyser[index].getFloatTimeDomainData(this.floatTimeDomain[index])
        return this.floatTimeDomain[index]
    }

    static getByteTimeDomainData(index=0) {
        this.analyser[index].getByteTimeDomainData(this.byteTimeDomain[index])
        return this.byteTimeDomain[index]
    }

    static getByteFrequencyData(index=0) {
        this.analyser[index].getByteFrequencyData(this.byteBuffers[index])
        return this.byteBuffers[index]
    }

    static getFloatFrequencyData(index=0) {
        this.analyser[index].getFloatFrequencyData(this.floatBuffers[index])
        return this.floatBuffers[index]
    }

}
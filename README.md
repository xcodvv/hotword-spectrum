## Hotword spectrogram building application
Application uses an audio input device to grab a frequency data and draws spectrogram with HTMLCanvasElement.\
Application development was inspired by TensorFlow hotword detection example which is not includes of how to build a spectrogram.

TensorFlow hotword detection example for microcontrollers: [Micro Speech Training](https://github.com/tensorflow/tensorflow/tree/master/tensorflow/lite/micro/examples/micro_speech/train)

Another example of hotword detection with Keras: [Real Time Trigger Word Detection with Keras](https://www.dlology.com/blog/how-to-do-real-time-trigger-word-detection-with-keras/)

Web-Application based on JavaScript with Vue3/Vuex/PrimeVue libraries.

## How to install
- `git clone https://gitlab.com/xcodvv/hotword-spectrum.git hotword-spectrum`
- `cd hotword-spectrum`
- `npm install`

## To run development environment
- `npm run dev`
- open the url `http://localhost:3000` on your favorite browser.

## To build the application
- `npm run build`

## To host a builded application
- `npm run serve`

## TODO
- make available to download a spectrogram from the canvas;
- add additional settings for AudioContext: channelsCount, channelIndex, sampleRate, minDecibels, maxDecibels and fftSize;
- add equalizer to control of frequency gains;
- add configuration store, for example based on localStorage;
- add settings and equalizer presets;
- implement a tool to build a short activation spectrograms;
- implement a wizard that builds a spectrogram with any length and makes overlay of activation chunks by button press;
- add to wizard spectrogram binary labels: 0-noise/fail and 1-activation;
- make available to download a ready to use dataset with labels and full length spectrogram;
